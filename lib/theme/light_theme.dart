import 'package:flutter/material.dart';

ThemeData light = ThemeData(
  fontFamily: 'Roboto',
  primaryColor: Color(0xFFFF0000),
  secondaryHeaderColor: Color(0xFF1ED7AA),
  disabledColor: Color(0xFF000000),
  backgroundColor: Color(0xFFF3F3F3),
  errorColor: Color(0xFFFF0000),
  brightness: Brightness.light,
  hintColor: Color(0xFF000000),
  cardColor: Colors.white,
  colorScheme: ColorScheme.light(primary: Color(0xFFFF0000), secondary: Color(
      0xFFFF0000)),
  textButtonTheme: TextButtonThemeData(style: TextButton.styleFrom(primary: Color(
      0xFFFF0000))),
);